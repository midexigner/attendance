import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme,CssBaseline  } from '@material-ui/core';
import Login from './pages/Login/Login';
import DailyAttendance from './pages/DailyAttendance';
import FourZeroFour from './pages/FourZeroFour';
import Forgot from './pages/ForgotPassword/Forgot';
import Dashboard from './pages/Dashboard/Dashboard';
import Empoylee from './pages/Employee/Index';
import Department from './pages/Department/Department';
import Designation from './pages/Designation/Designation';
import Settings from './pages/Settings/Settings';
import Localization from './pages/Settings/Localization';
import ThemeSettings from './pages/Settings/ThemeSettings';
import RolesPermissions from './pages/Settings/RolesPermissions';
import EmailSettings from './pages/Settings/EmailSettings';
import LeaveType from './pages/Settings/LeaveType';
import Notifications from './pages/Settings/Notifications';
import ClockCard from './components/ClockCard';
import MyProfile from './pages/MyProfile/MyProfile';
// import Upload from './pages/Upload';
const THEME = createMuiTheme({
  typography: {
   "fontFamily": `"Nunito", sans-serif`,
   "fontSize": 15,
   "fontWeightLight": 300,
   "fontWeightRegular": 400,
   "fontWeightMedium": 500
  }
});

const App = () => {
  
  return (
    <MuiThemeProvider theme={THEME}>
       <CssBaseline />
      
      <Router>
    <Switch>
      <Route exact path="/" component={Dashboard} />
      <Route exact path="/dashboard" component={Dashboard} />
      <Route exact path="/clock" component={ClockCard} />
      <Route exact path="/designation" component={Designation} />
      <Route exact path="/daily-attendance" component={DailyAttendance} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/forgot-password" component={Forgot} />
      <Route exact path="/employee" component={Empoylee} />
      <Route exact path="/profile" component={MyProfile} />
      <Route exact path="/department" component={Department} />
      <Route exact path="/settings" component={Settings} />
      <Route exact path="/settings/localization" component={Localization} />
      <Route exact path="/settings/theme-settings" component={ThemeSettings} />
      <Route exact path="/settings/roles-permissions" component={RolesPermissions} />
      <Route exact path="/settings/notifications" component={Notifications} />
      <Route exact path="/settings/email-settings" component={EmailSettings} />
      <Route exact path="/settings/leave-type" component={LeaveType} />
      <Route exact component={FourZeroFour} />
    </Switch>
  </Router>
    </MuiThemeProvider>
  )
}

export default App
