import React from "react";
import { NavLink } from "react-router-dom";

const SidebarItem = ({ Icon, title, url }) => {
  return (
    <NavLink exact={true} to={url ? url : "/"} className="sidebarOption" activeClassName="active">
      {Icon && <Icon className="sidebarOption__icon" />}
       <h3 className="sidebarOption__name">{title}</h3>
    </NavLink>
  );
};

export default SidebarItem;
