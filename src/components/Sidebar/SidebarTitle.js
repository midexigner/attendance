import React from 'react'
import {Typography} from '@material-ui/core'

const SidebarTitle = ({title}) => {
    return (
            <Typography className="sidebarTitle" variant="h6" >{title}</Typography>  
    )
}

export default SidebarTitle
