import { Avatar } from '@material-ui/core'
import React,{useContext} from 'react'
import {useLocation} from "react-router-dom";
import { AiFillDashboard } from 'react-icons/ai'
import SettingsRoundedIcon from '@material-ui/icons/SettingsRounded';
import BarChartRoundedIcon from '@material-ui/icons/BarChartRounded';
import ReceiptRoundedIcon from '@material-ui/icons/ReceiptRounded';
import GroupRoundedIcon from '@material-ui/icons/GroupRounded';
import CategoryIcon from '@material-ui/icons/Category';
import HomeIcon from '@material-ui/icons/Home';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import AlternateEmailOutlinedIcon from '@material-ui/icons/AlternateEmailOutlined';
import {FaBuilding,FaKey,FaGlobeEurope} from 'react-icons/fa'
import {HiPhotograph} from 'react-icons/hi'
import {FiThumbsUp} from 'react-icons/fi'
import {BsPencilSquare,BsLock} from 'react-icons/bs'
import {ImCogs} from 'react-icons/im'
import SidebarItem from './SidebarItem';
import SidebarTitle from './SidebarTitle';
import { Context } from "../../context/Context";

const Sidebar = () => {
    const { user, dispatch } = useContext(Context);
    let location = useLocation();
 
    const handleLogout = ()=>{
        dispatch({ type: "LOGOUT" });
      }
    return (
        <aside className="sidebar">
          {location.pathname === '/settings' || location.pathname === '/settings/localization'|| location.pathname === '/settings/theme-settings' || location.pathname === '/settings/roles-permissions'|| location.pathname ===  '/settings/email-settings' || location.pathname === '/settings/leave-type'|| location.pathname === '/settings/notifications' ?(
               <>
               <SidebarItem Icon={HomeIcon} title="Back to Home" />
               <SidebarTitle title="Settings" />
               <SidebarItem Icon={FaBuilding} title="Company Settings" url="/settings" />
               <SidebarItem Icon={AccessTimeIcon} title="Localization" url="/settings/localization" />
               <SidebarItem Icon={HiPhotograph} title="Theme Settings" url="/settings/theme-settings" />
               <SidebarItem Icon={FaKey} title="Roles &amp; Permission" url="/settings/roles-permissions" />
               <SidebarItem Icon={AlternateEmailOutlinedIcon} title="Email Settings" url="/settings/email-settings" />
               <SidebarItem Icon={FiThumbsUp} title="Approval Settings" url="/settings" />
               <SidebarItem Icon={BsPencilSquare} title="Invoice Settings" url="/settings" />
               <SidebarItem Icon={FaGlobeEurope} title="Notifications" url="/settings/notifications" />
               <SidebarItem Icon={BsLock} title="Changed Password" url="/settings" />
               <SidebarItem Icon={ImCogs} title="Leave Type" url="/settings/leave-type" />
               </>
           ):(
           <>
           <div className="sidebar__header">
           <Avatar alt={user && user.user.FirstName +' '+user.user.LastName}  src={user && user.user.avatar} width={30}  onClick={()=> handleLogout()} />
           <h2>Welcome, <br/>
           <span>{user && <span>{user.user.FirstName}{" "}{user.user.LastName}</span> }</span>
           </h2>
           </div>
           
           <SidebarTitle title="Menu" />
           <SidebarItem Icon={AiFillDashboard} title="Dashboard" url="/dashboard" />
           <SidebarItem Icon={GroupRoundedIcon} title="Employees" url="/employee"  />
           <SidebarItem Icon={CategoryIcon} title="Department" url="/department"/>
           <SidebarItem Icon={ReceiptRoundedIcon} title="Designation" url="/designation" />
           <SidebarItem Icon={ReceiptRoundedIcon} title="Payroll" />
           <SidebarItem Icon={BarChartRoundedIcon} title="Reports" />
           <SidebarItem Icon={SettingsRoundedIcon} title="Settings" url="/settings" />
           </>
           )}
        </aside>
    )
}

export default Sidebar