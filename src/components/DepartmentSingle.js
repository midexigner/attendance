import React, { useState,useEffect } from 'react'
import axios from "../services/axios";
import requests from '../services/requests'

const DepartmentSingle = ({id}) => {
  const [department,setDepartment] = useState('');
  useEffect(() => {
    async function fetchData() {
      try {
        const request = await axios.get(requests.fetchDepartment+"/"+id);
        setDepartment(request.data)
      return request.data;
      } catch (error) {
        console.error(error);
      }
    }

    fetchData();

  }, [id])
 
    return (
        <div>
            {department.name}
            {/* https://greensock.com/forums/topic/17846-animate-a-modal-inout-from-the-click-point-of-origin/ */}
        </div>
    )
}

export default DepartmentSingle
