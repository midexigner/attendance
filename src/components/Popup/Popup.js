import React from 'react'
import './Popup.css'
import {Dialog,DialogTitle,DialogContent,IconButton} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
const Popup = ({title,children,setOpenPopup,openPopup,onClose}) => {
    return (
        <Dialog fullWidth={true} maxWidth={'md'} open={openPopup} >
             {onClose ? (
        <IconButton aria-label="close" onClick={onClose} className="closeButton">
          <CloseIcon />
        </IconButton>
      ) : null}
          <DialogTitle className="popupTitle">{title}</DialogTitle> 
          <DialogContent dividers>
              {children}
          </DialogContent>
        </Dialog>
    )
}

export default Popup