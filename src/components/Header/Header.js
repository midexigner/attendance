import React,{useState,useContext} from 'react'
import './Header.css'
import {AppBar, Typography, Button, Toolbar, IconButton,Avatar,ClickAwayListener } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChatIcon from '@material-ui/icons/Chat';
import {Link} from "react-router-dom"
import { Context } from "../../context/Context";
const useStyles = makeStyles((theme) => ({
  userRoot: {
    position: 'relative',
  },
  userDropdown: {
    position: 'absolute',
    top: 58,
    right: 0,
    left: 0,
    zIndex: 1,
    padding: theme.spacing(1),
    backgroundColor: theme.palette.background.paper,
    border: '1px solid rgba(0, 0, 0, 0.1)',
    borderRadius: '3px',
    transformOrigin:' left top 0',
    boxShadow: "inherit",
 
  },
}));

 
const Header = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
    const { user, dispatch } = useContext(Context);
   
    const handleClick = () => {
      setOpen((prev) => !prev);
    };
  
    const handleClickAway = () => {
      setOpen(false);
    };
    const handleLogout = ()=>{
      // history.push("/login");
      dispatch({ type: "LOGOUT" });
    }
    

    return (
        <>
           <AppBar position="fixed">
           <Toolbar className="headerInner">
             <div className="headerLeft">
              <img src="https://smarthr-angular.dreamguystech.com/orange/assets/img/logo.png" alt="Logo" />
             </div>
           <div className="headerCenter">
           <IconButton edge="start"  color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
           <Typography variant="h6" >Ossols Pvt.Ltd</Typography>
           </div>
       <div className="headerRight">
         <ul>
           <li><Button><NotificationsIcon/></Button></li>
           <li><Button><ChatIcon/></Button></li>
           <ClickAwayListener
      mouseEvent="onMouseDown"
      touchEvent="onTouchStart"
      onClickAway={handleClickAway}
    >
           <li className={classes.userRoot}>
          <a href="/#" onClick={handleClick} rel="noopener noreferrer"> <Avatar alt={user && user.user.FirstName +' '+user.user.LastName}  src={user && user.user.avatar} width={30} />
             {user && <span>{user.user.FirstName}{" "}{user.user.LastName}</span> } <span className="downUpIcon">{open ? (<ExpandLessIcon/>):(<ExpandMoreIcon/>)}</span></a>
             {open ? (
          <div className={`userDropdown ${classes.userDropdown}`}>
           <ul>
               <li> <Link to="/profile" >My Profile</Link></li>
               <li> <Link to="/settings" >Settings</Link></li>
               <li> <a href="/#" onClick={handleLogout} rel="noopener noreferrer">Logout</a></li>
             </ul>
          </div>
        ) : null}
             
           </li>
           </ClickAwayListener>
         </ul>
       </div>
    </Toolbar>
           </AppBar>
        </>
    )
}

export default Header