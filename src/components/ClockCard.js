import React from 'react';
import '../styles/Clock.css';

function ClockCard() {
  const dateBuilder = (d , dd = false) => {
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    let day = days[d.getDay()];
    let date = d.getDate();
    let month = months[d.getMonth()];
    let year = d.getFullYear();
        if(dd === false) {
    return `${month} ${date}, ${year}`
    } else{
        return `${day}` 
    }
  }
    return (
        <div className="clockwrapper">
         
            <div class="timeclock">
                    <span id="show_day" class="clock-text">{dateBuilder(new Date(),true)}</span>
                    <span id="show_time" class="clock-time">16:49:37</span>
                    <span id="show_date" class="clock-day">{dateBuilder(new Date())}</span>
                </div>
        </div>
    )
}

export default ClockCard
