import { makeStyles, createStyles,Grid, Typography,Button } from '@material-ui/core'
import React,{useContext} from 'react'
import { Link ,Redirect} from 'react-router-dom';
import {BiPlusMedical} from 'react-icons/bi'
import Header from './Header/Header'
import Sidebar from './Sidebar/Sidebar'
import {Context} from '../context/Context'
const useStyles = makeStyles((theme: Theme) => createStyles({
    pageHeader: {
            marginBottom:theme.spacing(2)
    },
    addButton: {
      maxWidth:'200px',
      display:'flex',
      fontWeight: 500,
      color: '#fff',
      userSelect: 'none',
      border: '1px solid #ff9b44',
      backgroundColor:'#ff9b44',
      borderRadius:' 50px',
      fontSize:'16px',
      padding:'8px 18px',
      textTransform:'capitalize',
      '& svg':{
marginRight:'5px',
      },
      '&:hover':{
        backgroundColor:' #ff851a',
        border:" 1px solid #ff851a"
      }
    }

  }));

const Layout = ({children,title,url,addText,onClick}) => {
    const classes = useStyles();
    const { user } = useContext(Context);
    return (
      <>
      {!user ? <Redirect to={`/login`} noThrow />:null}
        <main className="wrapper">
        <Header />
           <div className="containArea">
            <Sidebar />
           <div className="content">
           <Grid container spacing={3} direction="row" justify="center" alignItems="center" className={classes.pageHeader}>
             <Grid item md={6} justify="flex-start" container>
             {title && <Typography gutterBottom variant="h5" className="pageTitle">
                 {title}
                </Typography>}
             </Grid>
             <Grid item md={6} justify="flex-end" container>
             {url && <Link to={url} className="addBtn"><BiPlusMedical/> Add {addText}</Link>}
             {onClick && <Button  className={classes.addButton} onClick={onClick}><BiPlusMedical/>{addText}</Button>} 
             </Grid>
             </Grid>
           {children}
           </div>
           </div>

        </main>
        </>
    )
}

export default Layout