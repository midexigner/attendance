import React, { useState,useEffect } from 'react'
import axios from "../services/axios";
import requests from '../services/requests'

const UserSingle = ({id}) => {
  const [user,setUser] = useState('');
  useEffect(() => {
    async function fetchData() {
      try {
        const request = await axios.get(requests.fetchUser+"/"+id);
        setUser(request.data)
      return request.data;
      } catch (error) {
        console.error(error);
      }
    }

    fetchData();

  }, [id])
 
    return (
        <div>
            {user.name}
            {/* https://greensock.com/forums/topic/17846-animate-a-modal-inout-from-the-click-point-of-origin/ */}
        </div>
    )
}

export default UserSingle
