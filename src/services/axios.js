import axios from 'axios';
/* 
* base url to make request to the database
*/

const instance = axios.create({
     baseURL:"https://miattendance.herokuapp.com/api/"
    //baseURL:"http://localhost:8000/api"

});

export default instance;