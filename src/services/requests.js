const requests = {
    fetchUser:`/user`,
    fetchDepartment:`/department`,
    fetchDesignation:`/designation`,
    fetchSetting:`/settings/60d2106757aca54b78af2c53`,
    authLogin:`/auth/login`,
}
export default requests;