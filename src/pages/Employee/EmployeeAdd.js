import { Grid,TextField,Box} from '@material-ui/core'
import React from 'react'


 const handleSubmit = (e)=>{
    e.preventDefault();
 }
const EmployeeAdd = () => {
  //const classes = useStyles();
  return (
    <>
         <form onSubmit={handleSubmit} autoComplete="off">
         <Grid container spacing={4}>
         <Grid item md={6} >
        {/* //being firstName */}
        <Box id="firstName" mb={3} component="div">
        <TextField
         required
         fullWidth={true}
         label="First Name"
         defaultValue=""
         // value={firstName}
         // onChange={onChangefirstName}
         variant="outlined"
        />
        </Box>
        {/* //end firstName */}
        {/* //being Username */}
        <Box id="Username" mb={3} component="div">
        <TextField
         required
         fullWidth={true}
         label="Username"
         defaultValue=""
         // value={username}
         // onChange={onChangeUsername}
         variant="outlined"
        />
        </Box>
        {/* //end Username */}
        {/* //being Password */}
        <Box id="Password" mb={3} component="div">
        <TextField
         required
         fullWidth={true}
         label="Password"
         defaultValue=""
         // value={password}
         // onChange={onChangePassword}
         variant="outlined"
         type='password'
        />
        </Box>
        {/* //end Password */}
        {/* //being EmployeeID */}
        <Box id="EmployeeID" mb={3} component="div">
        <TextField
         required
         fullWidth={true}
         label="Employee ID"
         defaultValue=""
         // value={employeeID}
         // onChange={onChangeEmployeeID}
         variant="outlined"
         type='tel'
        
        />
        </Box>
        {/* //end EmployeeID */}
        {/* //being Phone */}
        <Box id="Phone" mb={3} component="div">
        <TextField
         required
         fullWidth={true}
         label="Phone"
         defaultValue=""
         // value={phone}
         // onChange={onChangePhone}
         variant="outlined"
         type='tel'
        />
        </Box>
        {/* //end Phone */}
         

         </Grid>
         <Grid item md={6} >
         {/* // being lastName */}
         <Box id="lastName" mb={3} component="div">
         <TextField
         fullWidth={true}
          label="Last Name"
          // value={lastName}
          // onChange={onChangelastName}
          variant="outlined"
        />
        </Box>
         {/* // end lastName */}
          {/* // being Email */}
          <Box id="Email" mb={3} component="div">
          <TextField
         fullWidth={true}
          label="Email"
          type='email'
          // value={email}
          // onChange={onChangeEmail}
          variant="outlined"
        />
        </Box>
         {/* // end Email */}
           {/* // being ConfirmPassword */}
           <Box id="ConfirmPassword" mb={3} component="div">
          <TextField
          type='password'
         fullWidth={true}
          label="Confirm Password"
          // value={confirmPassword}
          // onChange={onChangeConfirmPassword}
          variant="outlined"
        />
        </Box>
         {/* // end ConfirmPassword */}
         {/* // being JoinDate */}
         <Box id="JoinDate" mb={3} component="div">
          <TextField
         fullWidth={true}
          label="Join Date"
          type='date'
          InputLabelProps={{
            shrink: true,
          }}
          // value={joinDate}
          // onChange={onChangeJoinDate}
          variant="outlined"
        />
        </Box>
         {/* // end JoinDate */}
         </Grid>
         </Grid>
         </form>
    </>
    )
}

export default EmployeeAdd