import React,{useState,useEffect} from 'react'
import Layout from '../../components/Layout'
import { makeStyles, createStyles,Paper,Table, TableBody, TableContainer, TableHead,TableFooter, TableRow,TableCell, TablePagination, IconButton,Avatar } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import EmployeeAdd from './EmployeeAdd'
import Popup from '../../components/Popup/Popup'
import axios from "../../services/axios";
import requests from "../../services/requests";

const Employee = () => {
  const [lists,setLists] = useState([{}])
  const [openPopup, setOpenPopup] = useState(false);
  const useStyles = makeStyles((theme: Theme) => createStyles({
    miIcon: {
      padding:'0px',
      fontSize:'12px',
      marginLeft:'8px',
      '& svg':{
        fontSize:'16px'
      }
    },
   tableTd:{
      padding:'8px 16px'
    }

  }));
 
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(6);
   
    useEffect(() => {
      async function fetchData() {
        try {
          const request = await axios.get(requests.fetchUser);
          //console.log(request.data)
          setLists(request.data);
          return request.data;
        } catch (error) {
          console.error(error);
        }
      }
  
      fetchData();
  
    }, [])
        const handleChangePage = (event, newPage) => {
          setPage(newPage);
        };
      
        const handleChangeRowsPerPage = (event) => {
          setRowsPerPage(+event.target.value);
          setPage(0);
        };

        const classes = useStyles();
      return (
          <Layout title="Manage Employees" onClick={()=>setOpenPopup(true)} addText="Add Employee">
             <TableContainer component={Paper}>
                 <Table aria-label="custom pagination table">
                 <TableHead>
                     <TableRow>
                         <TableCell>#</TableCell>
                         <TableCell>Name</TableCell>
                         <TableCell>Employee ID</TableCell>
                         <TableCell>Email</TableCell>
                         <TableCell>Mobile</TableCell>
                         <TableCell>Join Date</TableCell>
                         <TableCell>Role</TableCell>
                         <TableCell>Action</TableCell>
                     </TableRow>
                 </TableHead>
                 <TableBody>
                 {lists.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row,index) => (
                   <TableRow key={index}>
                   <TableCell className={classes.tableTd}>{index + 1}</TableCell>
             <TableCell className={classes.tableTd}><span style={{display:'flex',alignItems: 'center'}}>
               <Avatar alt={`${row.FirstName} ${row.LastName}`}  src={row && row.avatar} width={24} />
                {`${row.FirstName} ${row.LastName}`}</span></TableCell>
                   <TableCell className={classes.tableTd}>{row.EmployeeCode}</TableCell>
                   <TableCell className={classes.tableTd}>{row.email}</TableCell>
                   <TableCell className={classes.tableTd}>{row.ContactNo}</TableCell>
                   <TableCell className={classes.tableTd}>{row.department}</TableCell>
                   <TableCell className={classes.tableTd}>{row.role}</TableCell>
                   <TableCell className={classes.tableTd}>
                     <IconButton color="inherit" className={classes.miIcon}><VisibilityIcon/></IconButton>
                     <IconButton color="inherit"  className={classes.miIcon}><EditIcon/></IconButton>
                     <IconButton color="inherit" className={classes.miIcon}><DeleteIcon/></IconButton>
                     </TableCell>
               </TableRow>
                 ))}
                 </TableBody>
                 <TableFooter>
                   <TableRow>
                   <TablePagination
          rowsPerPageOptions={[10, 50, 100,999]}
          component="td"
          count={lists.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
                   </TableRow>
                   </TableFooter>
                 </Table>
             </TableContainer>
             <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}  title="Add Employee" onClose={()=>setOpenPopup(false)}>
<EmployeeAdd />
             </Popup>
          </Layout>
    )
}

export default Employee
