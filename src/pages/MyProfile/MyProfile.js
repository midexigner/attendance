import React,{useState} from "react";
import Layout from "../../components/Layout";
import { Grid, Avatar, Box, Typography } from "@material-ui/core";
import EditIcon from '@material-ui/icons/Edit';
import Popup from '../../components/Popup/Popup'
const MyProfile = () => {
    const [openPopup, setOpenPopup] = useState(false);
    const user = JSON.parse(localStorage.getItem("user"))
  return (
    <Layout title="Profile">
      <Box className="card">
        <Box className="card-body">
          <Grid container>
            <Box className="profile-view">
              <Box className="profile-img-wrap">
                <Box className="profile-img">
                  <a href="/#" rel="noopener noreferrer">
                    <Avatar
                      component="div"
                      alt={user && user.user.FirstName +' '+user.user.LastName}
                      src={user && user.user.avatar}
                    />
                  </a>
                </Box>
              </Box>
              <Box className="profile-basic">
                <Grid container spacing={3}>
                  <Grid item md={5}>
                    <Box className="profile-info-left">
                    {user &&<Typography variant="h3" component="div">
                      {user.user.FirstName}{" "}{user.user.LastName}
                      </Typography>}
                      <Typography variant="h6" component="div">
                        RUI/UX Design Team{" "}
                      </Typography>
                      <Typography variant="subtitle1" component="div">
                        Web Designer
                      </Typography>
                      {user &&<Typography variant="caption" component="div">
                        Employee ID : {user.user.EmployeeCode}
                      </Typography>}
                      <Typography variant="subtitle2" component="div">
                        Date of Join : 1st Jan 2021
                      </Typography>
                      <Box className="staff-msg ">
                        <a
                          className="addBtn btn-custom"
                          href="/#"
                          rel="noopener noreferrer"
                        >
                          Send Message
                        </a>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item md={7}>
                    <ul className="personal-info">
                    {user &&<li>
                        <div className="title">Phone:</div>
                        <div className="text">
                          <a  href="/#"
                          rel="noopener noreferrer">{user.user.ContactNo}</a>
                        </div>
                      </li>}
                     {user && <li>
                        <div className="title">Email:</div>
                        <div className="text">
                          <a  href="/#"
                          rel="noopener noreferrer">{user.user.email}</a>
                        </div>
                      </li>}
                      {user &&<li>
                        <div className="title">Birthday:</div>
                        <div className="text">24th July</div>
                      </li>}
                      {user &&<li>
                        <div className="title">Address:</div>
                        <div className="text">
                          1861 Bayonne Ave, Manchester Township, NJ, 08759
                        </div>
                      </li>}
                      {user &&<li>
                        <div className="title">Gender:</div>
                        <div className="text">{user.user.gender}</div>
                      </li>}
                      {user &&<li>
                        <div class="title">Reports to:</div>
                        <div className="text">
                          <div class="avatar-box">
                            <div className="avatar avatar-xs">
                              <img
                                src="https://res.cloudinary.com/mi-dexigner/image/upload/v1634649283/profile_r1l7ub.jpg"
                                alt=""
                              />
                            </div>
                          </div>
                          <a  href="/#"
                          rel="noopener noreferrer">
                            Jeffery Lalor
                          </a>
                        </div>
                      </li>}
                    </ul>
                  </Grid>
                </Grid>
              </Box>
              <Box className="pro-edit">
                <EditIcon onClick={()=>setOpenPopup(true)} />
              </Box>
              {/* being popup Profile Edit */}
              <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}  title="Profile Information" onClose={()=>setOpenPopup(false)}>
Edit Profile Form
             </Popup>
              {/* end popup Profile Edit */}
            </Box>
          </Grid>
        </Box>
      </Box>
      <Box className="pro-overview">
      <Grid container spacing={2}>
      <Grid item md={6}>
        <Box className="card">
        <Box className="card-body">
        <Box className="pro-edit">
                <EditIcon onClick={()=>setOpenPopup(true)} />
              </Box>
        <Typography variant="h3" component="div">
        Personal Informations</Typography>
                      <ul className="personal-info">
                      <li>
                        <div className="title">Passport No.</div>
                        <div className="text">9876543210</div>
                      </li>
                      <li>
                        <div className="title">Passport Exp Date.</div>
                        <div className="text">9876543210</div>
                      </li>
                      <li>
                        <div className="title">Tel.</div>
                        <div className="text">9876543210</div>
                      </li>
                      <li>
                        <div className="title">Nationality.</div>
                        <div className="text">Pakistani</div>
                      </li>
                      <li>
                        <div className="title">Religion.</div>
                        <div className="text">Islam</div>
                      </li>
                      <li>
                        <div className="title">Employment of spouse.</div>
                        <div className="text">No</div>
                      </li>
                      <li>
                        <div className="title">No. of children.</div>
                        <div className="text">0</div>
                      </li>
                      </ul>
        </Box>
        </Box>
      </Grid>
      <Grid item md={6}>
      <Box className="card">
        <Box className="card-body">
        <Box className="pro-edit">
                <EditIcon onClick={()=>setOpenPopup(true)} />
              </Box>
        <Typography variant="h3" component="div">
        Emergency Contact</Typography>
                      <ul className="personal-info info-personal2">
                      <li><div className="title">Primary</div></li>
                      <li>
                        <div className="title">Name.</div>
                        <div className="text">Lal Khan</div>
                      </li>
                      <li>
                        <div className="title">Relationship.</div>
                        <div className="text">Father</div>
                      </li>
                      <li>
                        <div className="title">Phone.</div>
                        <div className="text">9876543210</div>
                      </li>
                      </ul>
                      <hr className="sm-divider" />
                      <ul className="personal-info">
                      <li>
                        <div className="title">Secondary</div>
                      </li>
                      <li>
                        <div className="title">Name.</div>
                        <div className="text">Lal Khan</div>
                      </li>
                      <li>
                        <div className="title">Relationship.</div>
                        <div className="text">Father</div>
                      </li>
                      <li>
                        <div className="title">Phone.</div>
                        <div className="text">9876543210</div>
                      </li>
                      </ul>
        </Box>
        </Box>
      </Grid>
      </Grid>
      </Box>
    </Layout>
  );
};

export default MyProfile;
