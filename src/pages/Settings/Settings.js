import React, { useEffect,useState } from 'react'
import { makeStyles, createStyles,Box, Grid, TextField, Button } from '@material-ui/core'
import Layout from '../../components/Layout'
import axios from "../../services/axios";
import requests from "../../services/requests";

const Settings = (props) => {
  const [company, setCompany] = useState('')
  const [address, setAddress] = useState('')
  const [country, setCountry] = useState('')
  const [city, setCity] = useState('')
  const [state, setState] = useState('')
  const [postalCode, setPostalCode] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [mobile, setMobile] = useState('')
  const [fax, setFax] = useState('')
  const [websiteURL, setWebsiteURL] = useState('')
  const [userId, setUserId] = useState('')
    const useStyles = makeStyles((theme: Theme) => createStyles({
       inputField:{
          maxWidth:'auto'
        },
       inputFieldButton:{
          maxWidth:'200px',
          display:'inline-block',
          fontWeight: 600,
          color: '#fff',
          userSelect: 'none',
          border: '1px solid #ff9b44',
          backgroundColor:'#ff9b44',
          borderRadius:' 50px',
          fontSize:'18px',
          '&:hover':{
            backgroundColor:' #ff851a',
            border:" 1px solid #ff851a"
          }
        },settingsWrapper:{
          maxWidth:'1100px',
          margin:'auto'
        }
    }));
  useEffect(()=>{
    async function fetchData() {
      try {
        const request = await axios.get(requests.fetchSetting);
        setCompany(request.data.company_name);
        setAddress(request.data.address);
        setCountry(request.data.country);
        setCity(request.data.city);
        setPostalCode(request.data.postal_code);
        setState(request.data.state);
        setEmail(request.data.email);
        setPhone(request.data.phone);
        setMobile(request.data.mobile);
        setFax(request.data.fax);
        setWebsiteURL(request.data.website_url);
        setUserId(request.data.user_id[0]);
        // console.log(request.data);
        // return request.data;
      } catch (error) {
        console.error(error);
      }
    }
    fetchData();
  },[])
  const onChangeCompany = (event) => {
    setCompany( event.target.value);
};
  const onChangeAddress = (event) => {
    setAddress( event.target.value);
};
  const onChangeCountry = (event) => {
    setCountry( event.target.value);
};
  const onChangeCity = (event) => {
    setCity( event.target.value);
};
  const onChangePostalCode = (event) => {
    setPostalCode( event.target.value);
};
  const onChangeState = (event) => {
    setState( event.target.value);
};
  const onChangeEmail = (event) => {
    setEmail( event.target.value);
};
  const onChangePhone = (event) => {
    setPhone( event.target.value);
};
  const onChangeMobile = (event) => {
    setMobile( event.target.value);
};
  const onChangeFax = (event) => {
    setFax( event.target.value);
};
  const onChangeWebsiteURL = (event) => {
    setWebsiteURL( event.target.value);
};

    const handleUpdate = (e)=>{
    e.preventDefault();
    // console.log(company)
    // console.log(address)
    // console.log(country)
    // console.log(city)
    // console.log(state)
    // console.log(postalCode)
    // console.log(email)
    // console.log(phone)
    // console.log(mobile)
    // console.log(fax)
    // console.log(websiteURL)
    // console.log(userId)
    }
   
    const classes = useStyles();
    return (
        <Layout title="Settings">
            <Box >
  <form onSubmit={handleUpdate}>
             <Grid container spacing={3} className={classes.settingsWrapper}>
             <Grid item md={6}>
                  {/* being Site Title field */}
             <Box id="siteTitle" mb={3} component="div">
            <TextField
          label="Company Name"
          value={company}
          onChange={onChangeCompany}
          variant="outlined"
        />
            <input
          type="hidden"
          value={userId}
          variant="outlined"
        />
             </Box>
             {/* end Site Title field */}
                  
             </Grid>
             <Grid item md={6}>
                 {/* being Company Name field */}
             {/* <Box id="companyName" mb={3} component="div"> */}
             {/* <TextField variant="outlined" label="Company Name" type="text" className={classes.inputField} fullWidth value={settings.company_name} onChange={(e)=>setCompany(e)}  /> */}
             {/* </Box> */}
             {/* end Company Name field */}
                  
             </Grid>
             <Grid item md={12}>
               {/* being address field */}
               <Box id="address" mb={3} component="div">
             <TextField variant="outlined" label="Address" type="text" className={classes.inputField} fullWidth
              value={address}
              onChange={onChangeAddress}
             />
             </Box>
             {/* end address field */}
             </Grid>
                  {/* being Phone field */}
                  <Grid item md={3}>
                    {/* being Country field */}
             <Box id="country" mb={3} component="div">
             <TextField variant="outlined" label="Country*" type="text" className={classes.inputField} fullWidth
              value={country}
              onChange={onChangeCountry}
              />
             </Box>
             {/* end Country field */}
                  </Grid>
                  <Grid item md={3}>
                    {/* being City* field */}
             <Box id="city" mb={3} component="div">
             <TextField variant="outlined" label="City*" type="text" className={classes.inputField} fullWidth
              value={city}
              onChange={onChangeCity}
             />
             </Box>
             {/* end City* field */}
             
                  </Grid>
                  <Grid item md={3}>
                     {/* being State* field */}
             <Box id="state*" mb={3} component="div">
             <TextField variant="outlined" label="State*" type="text" className={classes.inputField} fullWidth
              value={state}
              onChange={onChangeState}
             />
             </Box>
             {/* end State* field */}
             
                  </Grid>
                  <Grid item md={3}>
                     {/* being Postal Code* field */}
             <Box id="postalCode" mb={3} component="div">
             <TextField variant="outlined" label="Postal Code*" type="text" className={classes.inputField} fullWidth
              value={postalCode}
              onChange={onChangePostalCode} />
             </Box>
             {/* end Postal Code* field */}
                  </Grid>
              <Grid item md={6}>
                 {/* being Email field */}
             <Box id="email" mb={3} component="div">
             <TextField variant="outlined" label="Email*" type="email" className={classes.inputField} fullWidth
              value={email}
              onChange={onChangeEmail}
              />
             </Box>
             {/* end Email field */}
              </Grid>
              <Grid item md={6}>
                 {/* being Phone* field */}
              <Box id="phone" mb={3} component="div">
             <TextField variant="outlined" label="Phone" type="text" className={classes.inputField} fullWidth
             value={phone}
             onChange={onChangePhone}
             />
             </Box>
             {/* end Phone field */}
              </Grid>
              <Grid item md={6}>
                {/* being Mobile* field */}
              <Box id="Mobile" mb={3} component="div">
             <TextField variant="outlined" label="Mobile" type="text" className={classes.inputField} fullWidth
             value={mobile}
             onChange={onChangeMobile}
              />
             </Box>
             {/* end Mobile field */}
              </Grid>
              <Grid item md={6}>
              {/* being Fax* field */}
              <Box id="Fax" mb={3} component="div">
             <TextField variant="outlined" label="Fax*" type="text" className={classes.inputField} fullWidth
             value={fax}
             onChange={onChangeFax}
             />
             </Box>
             {/* end Fax* field */}
              </Grid>
              <Grid item md={12}>
              {/* being Website URL* field */}
             <Box id="websiteURL" mb={3} component="div">
             <TextField variant="outlined" label="Website URL*" type="text" className={classes.inputField} fullWidth
              value={websiteURL}
              onChange={onChangeWebsiteURL}
             />
             </Box>
             </Grid>
             {/* end Website URL* field */}
             </Grid>
            
             <Grid item md={12}>
             <Box flexWrap="nowrap" alignItems="center" justifyContent="center" display='flex' component="section">  <Button variant="contained" color="primary" type="submit" className={classes.inputFieldButton} size="large" disableElevation  fullWidth >Save</Button></Box>
             </Grid>
            
             </form>

</Box>
             
        </Layout>
    )
}

export default Settings
