import React,{ useRef,useContext } from 'react'
import { Box, Card, CardContent, Container,Typography,makeStyles, createStyles, Avatar, TextField, Button } from '@material-ui/core';
import { Link,Redirect } from 'react-router-dom';
import {Context} from '../../context/Context'
import axios from "../../services/axios";
import requests from "../../services/requests";

const useStyles = makeStyles((theme: Theme) => createStyles({
    mainWrapper: {
      height:'100vh',
      textAlign:'center',
      fontSize: '.9375rem',
      color: "#1f1f1f",
      backgroundColor: "#f7f7f7",
      minHeight: "100%",
      overflowX:"hidden"
      
    },
    logo: {
        width: theme.spacing(13),
        height: theme.spacing(13),
        marginLeft:'auto',
        marginRight:'auto',
        marginBottom:theme.spacing(4)
      },
      loginBox:{
        backgroundColor: '#fff',
        border: "1px solid #ededed",
        borderRadius: "4px",
        boxShadow:" 0 1px 1px 0 rgba(0, 0, 0, 0.2)",
        margin: "0 auto",
        overflow: "hidden",
        width: "480px",
      },
      heading:{
        fontSize:'26px',
        fontWeight: 500,
        marginBottom:theme.spacing(1),
      },
      subtitle:{
        fontSize:'18px',
        color:'#4c4c4c',
        marginBottom:theme.spacing(2),
      },
     textMuted:{
      color:'#8e8e8e',
      textDecoration:'none',
      fontSize:'.9375rem'
      },
     inputFieldButton:{
      maxWidth:'200px',
      display:'inline-block',
      fontWeight: 600,
      color: '#fff',
      userSelect: 'none',
      border: '1px solid #ff9b44',
      backgroundColor:'#ff9b44',
      borderRadius:' 50px',
      fontSize:'18px',
      '&:hover':{
        backgroundColor:' #ff851a',
        border:" 1px solid #ff851a"
      }
      }
  }));

const Login = () => {
    const classes = useStyles();
    const { dispatch,isFetching,user} = useContext(Context);
    const userRef = useRef();
    const passwordRef = useRef();

    //console.log(user)
    const handleSubmit = async (e)=>{
      e.preventDefault();
      dispatch({ type: "LOGIN_START" });
      try {

        const res = await axios.post(requests.authLogin, {
          email: userRef.current.value,
          password: passwordRef.current.value,
        });
        dispatch({ type: "LOGIN_SUCCESS", payload: res.data });
      } catch (err) {
        dispatch({ type: "LOGIN_FAILURE" });
       console.log("error");
      }
   
    }
    //console.log()
    return (
      <>
      {user ? <Redirect to={`/dashboard`} noThrow />:null}
        <Box className={classes.mainWrapper} flexWrap="nowrap" alignItems="center" justifyContent="center" display='flex' component="section">
        <Container maxWidth='sm'>
        <Avatar alt="Remy Sharp" src="https://smarthr-angular.dreamguystech.com/orange/assets/img/logo2.png" className={classes.logo} />
            <Card className={classes.loginBox}>
              
            <CardContent>
            
            <Typography variant="h5" component="div" className={classes.heading}>Login</Typography>
            <Typography variant="subtitle1" component="div" className={classes.subtitle}>Access to our dashboard</Typography>
            <form onSubmit={handleSubmit}>
             {/* being email field */}
             <Box id="email" mb={3} component="div">
             <TextField variant="outlined" label="Email Address" type="email" className={classes.inputField} fullWidth inputRef={userRef} />
             </Box>
             {/* end email field */}
             {/* being password field */}
             <Box id="password" mb={3} component="div">
             <TextField variant="outlined" label="Password" type="password" className={classes.inputField} fullWidth inputRef={passwordRef} />
             </Box>
             {/* end password field */}
              {/* being form submit */}
             <Button variant="outlined" color="primary" type="submit" className={classes.inputFieldButton} size="large" disableElevation  fullWidth  disabled={isFetching}>LogIn</Button>
             {/* end form submit */}
             <Box mt={1}  mb={3} component="div" width={1}>
                 <Link to="/forgot-password" className={classes.textMuted}>Forgot Password</Link>
             </Box>
             </form>
            </CardContent>
            </Card>
        </Container>
            </Box>
            </>
    )
}

export default Login
