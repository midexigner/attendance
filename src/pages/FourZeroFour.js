import React from 'react'
import Layout from '../components/Layout'

const FourZeroFour = () => {
  return (
    <Layout>
      404 page
    </Layout>
  )
}

export default FourZeroFour
