import React from 'react'
import { makeStyles, Paper, Box, Typography} from '@material-ui/core'
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));
const InfoWidget = ({Icon,count,title}) => {
    const classes = useStyles();
    return (
        <Paper  className={`tile-stats ${classes.paper}`}>
        <Box  display='flex' flexWrap="nowrap" alignItems="centers" justifyContent="space-between" component="span">
       <div className="dashWidgetIcon"><Icon /></div>
            <div>
              <div className="count">{count}</div> 
              <Typography variant="h6" component="div" className="title">{title}</Typography>
            </div>
        </Box>
   
    </Paper>
    )
}

export default InfoWidget
