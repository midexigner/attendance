import React from 'react'
import Layout from '../../components/Layout'
import SimpleBarChart from '../../components/SimpleBarChart/SimpleBarChart'
import { userData } from "../../dummyData";
import InfoWidget from './InfoWidget'
import {Grid } from '@material-ui/core'
import {FaUser,FaDollarSign} from 'react-icons/fa'
import {GrDiamond} from 'react-icons/gr'

const Dashboard = () => {
  
    return (
        <Layout title="Welcome Admin!">
           <Grid container spacing={3}>
        <Grid item md={3}>
          <InfoWidget Icon={FaUser} count="112" title="Projects" />
        </Grid>
        <Grid item md={3}>
        <InfoWidget Icon={FaDollarSign} count="44" title="Clients" />
        </Grid>
        <Grid item md={3}>
        <InfoWidget Icon={GrDiamond} count="30" title="Task" />
        </Grid>
        <Grid item md={3}>
        <InfoWidget Icon={FaUser} count="215" title="Employees" />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item md={12}>
          <SimpleBarChart data={userData} title="User Analytics" grid dataKey="Active User" />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item md={6}>
          Widget LG
        </Grid>
        <Grid item md={6}>
         Widget SM
        </Grid>
      </Grid>
        </Layout>
    )
}

export default Dashboard