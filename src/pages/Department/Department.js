import React,{useState,useEffect} from 'react'
import Layout from '../../components/Layout'
import { makeStyles, createStyles,Paper,Table, TableBody, TableContainer, TableHead,TableFooter, TableRow,TableCell, TablePagination, IconButton } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import AddDepartment from '../Department/AddDepartment'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Popup from '../../components/Popup/Popup'
import axios from "../../services/axios";
import requests from "../../services/requests";

const Department = () => {
  const useStyles = makeStyles((theme: Theme) => createStyles({
    miIcon: {
      padding:'0px',
      fontSize:'12px',
      marginLeft:'8px',
      '& svg':{
        fontSize:'16px'
      }
    },
   tableTd:{
      padding:'8px 16px'
    }

  }));
  
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [lists,setLists] = useState([{}])
    const [openPopup, setOpenPopup] = useState(false);
    const [del, setDelete] = useState('');

const handleDelete = async (del) =>{
  setDelete(del)
  const request = await axios.delete(requests.fetchDepartment+'/'+del);
  //alert(del)
  if(request){
    console.log("delete")
    
  }
}
      useEffect(() => {
      async function fetchData() {
        try {
          const request = await axios.get(requests.fetchDepartment);
        setLists(request.data);
      //  setTimeout(lists, 200); // request again after a minute
          return request.data;
        } catch (error) {
          console.error(error);
        }
      }
  
      fetchData();
  
    }, [lists])
    
        const handleChangePage = (event, newPage) => {
          setPage(newPage);
        };
      
        const handleChangeRowsPerPage = (event) => {
          setRowsPerPage(+event.target.value);
          setPage(0);
        };

        const classes = useStyles();
        
      return (
          <Layout title="Department" onClick={()=>setOpenPopup(true)} addText="Add Department">
            {del && <><Alert severity="error">Delete Successful Department</Alert></>}
             <TableContainer component={Paper}>
                 <Table aria-label="custom pagination table">
                 <TableHead>
                     <TableRow>
                         <TableCell variant="head" align="left">#</TableCell>
                         <TableCell variant="head" align="left">Name</TableCell>
                         <TableCell variant="head" align="left">Department Head</TableCell>
                         <TableCell variant="head" align="right">Action</TableCell>
                     </TableRow>
                 </TableHead>
                 <TableBody>
                 {lists && lists.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row,index) => (
                   <TableRow key={index}>
                   <TableCell className={classes.tableTd} variant="head" align="left">{index + 1}</TableCell>
                   <TableCell className={classes.tableTd} variant="head" align="left">{row.name}</TableCell>
                   <TableCell className={classes.tableTd} variant="head" align="left">ABC</TableCell>
                   <TableCell className={classes.tableTd} variant="head" align="right">
                     <IconButton color="inherit"  className={classes.miIcon}><EditIcon/></IconButton>
                     <IconButton color="inherit" className={classes.miIcon} onClick={()=>handleDelete(row._id)}><DeleteIcon/></IconButton>
                     </TableCell>
               </TableRow>
                 ))}
                 </TableBody>
                 <TableFooter>
                   <TableRow>
                   <TablePagination
          rowsPerPageOptions={[10, 50, 100,999]}
          component="td"
          count={lists.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
                   </TableRow>
                   </TableFooter>
                 </Table>
             </TableContainer>
             <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}  title="Add Department" onClose={()=>setOpenPopup(false)}>
<AddDepartment setOpenPopup={setOpenPopup} />
             </Popup>
          </Layout>
    )
}

export default Department
