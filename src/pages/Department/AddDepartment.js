import React, { useState } from 'react'
import {Box, Grid, TextField, Button } from '@material-ui/core'
import axios from "../../services/axios";
import requests from "../../services/requests";

const AddDepartment = ({setOpenPopup}) => {
    const user = JSON.parse(localStorage.getItem("user"))
    const [title,setTitle] = useState(); 
    const [user_id] = useState(user.user._id);
    const onChangeTitle = (event) => {
        setTitle( event.target.value);
    };
    const handleSubmit = async (e) =>{
        e.preventDefault();
        const newDepartment = {
            user_id:user_id,
            name:title,
            status:"active"
        }
        await axios.post(requests.fetchDepartment,newDepartment);
       setOpenPopup(false);
       setTitle('')
    }
    return (
        <Box>
            <form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
                
             <Grid item md={12}>
             <Box id="siteTitle" mb={3} component="div">
            <TextField
          label="Department Name"
          variant="outlined"
          fullWidth
          onChange={onChangeTitle}
        />
           
             </Box>
             {/* end Site Title field */}
             <Box flexWrap="nowrap" alignItems="center" justifyContent="center" display='flex' component="div">  
             <Button variant="contained" className="addBtn submit-btn" type="submit"  size="large" disableElevation >Submit</Button></Box>
             </Grid>
             </Grid>
             </form> 
        </Box>
    )
}

export default AddDepartment
