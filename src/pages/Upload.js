import React, { useState } from 'react'

function Upload() {
    const [fileInputState,setFileInputState] = useState('')
    const [previewSource,setPreviewSource] = useState('')
    const [selectedFile,setSelectedFile] = useState('')
    const handleFileInputChange = (e) =>{
        const file = e.target.files[0];
        previewFile(file);
    }

    const previewFile = (file)=>{
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = ()=>{
            setPreviewSource(reader.result)
        }
    }

    const handleSubmitFile = (e)=>{
        e.preventDefault();
        if(!selectedFile) return;
        const reader = new FileReader();
        reader.readAsDataURL(selectedFile)
    }
    return (
        <div>
            <h2>Upload</h2>
            <from onSubmit={handleSubmitFile}>
            <input type="file" name="image" onChange={handleFileInputChange} value={fileInputState} />
            <button type="button">Submit</button>
            </from>
            <br/>
            {previewSource && (
                <img src={previewSource} style={{height:'300px'}} alt=""/>
            )}
        </div>
    )
}

export default Upload
