import React from 'react'
import { Box, Card, CardContent, Container,Typography,makeStyles, createStyles, Avatar, TextField, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) => createStyles({
  mainWrapper: {
    height:'100vh',
    textAlign:'center',
    fontSize: '.9375rem',
    color: "#1f1f1f",
    backgroundColor: "#f7f7f7",
    minHeight: "100%",
    overflowX:"hidden"
    
  },
  logo: {
      width: theme.spacing(13),
      height: theme.spacing(13),
      marginLeft:'auto',
      marginRight:'auto',
      marginBottom:theme.spacing(4)
    },
    loginBox:{
      backgroundColor: '#fff',
      border: "1px solid #ededed",
      borderRadius: "4px",
      boxShadow:" 0 1px 1px 0 rgba(0, 0, 0, 0.2)",
      margin: "0 auto",
      overflow: "hidden",
      width: "480px",
    },
    heading:{
      fontSize:'26px',
      fontWeight: 500,
      marginBottom:theme.spacing(1),
    },
    subtitle:{
      fontSize:'18px',
      color:'#4c4c4c',
      marginBottom:theme.spacing(2),
    },
   textMuted:{
    color:'#8e8e8e',
    textDecoration:'none',
    fontSize:'.9375rem'
    },
   inputFieldButton:{
    maxWidth:'200px',
    display:'inline-block',
    fontWeight: 600,
    color: '#fff',
    userSelect: 'none',
    border: '1px solid #ff9b44',
    backgroundColor:'#ff9b44',
    borderRadius:' 50px',
    fontSize:'18px',
    '&:hover':{
      backgroundColor:' #ff851a',
      border:" 1px solid #ff851a"
    }
    }
}));
  
    const Forgot = () => {
      const classes = useStyles();

    const handleSubmit = (e)=>{
    e.preventDefault();
    alert();
    }
    return (
        
            <Box className={classes.mainWrapper} flexWrap="nowrap" alignItems="center" justifyContent="center" display='flex' component="section">
          
        <Container maxWidth='sm'>
        <Avatar alt="Remy Sharp" src="https://smarthr-angular.dreamguystech.com/orange/assets/img/logo2.png" className={classes.logo} />
            <Card>
            <CardContent>
            
            <Typography variant="h5" component="div" className={classes.heading}>Forgot Password?</Typography>
            <Typography variant="subtitle1" component="div" className={classes.subtitle}>Enter your email to get a password reset link</Typography>
            <form onSubmit={handleSubmit}>
             {/* being email field */}
             <Box id="email" mb={3} component="div">
             <TextField variant="outlined" label="Email Address" type="email" className={classes.inputField} fullWidth />
             </Box>
             {/* end email field */}
              {/* being form submit */}
             <Button variant="outlined" color="primary" type="submit" className={classes.inputFieldButton} size="large" disableElevation  fullWidth> Reset Password</Button>
             {/* end form submit */}
             <Box mt={1}  mb={3} component="div" width={1}>
             Remember your password? <Link to="/login" className={classes.textMuted}>Login</Link>
             </Box>
             </form>
            </CardContent>
            </Card>
        </Container>
            </Box>
    )
}

export default Forgot