import React,{useState,useEffect}from 'react'
import {Box, Grid, TextField, Button} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete';
import requests from '../../services/requests';
import axios from '../../services/axios';

const AddDesignation = ({setOpenPopup}) => {
  const [department, setDepartment] = useState({})
  const [value, setValue] = useState(null);
  const [id, setId] = useState(null);
  const user = JSON.parse(localStorage.getItem("user"))
  const [user_id] = useState(user.user._id);

  useEffect(() => {
    async function fetchData() {
      try {
        const request = await axios.get(requests.fetchDepartment);
        // console.log(request.data);
         setDepartment(request.data)
         return request.data;
      } catch (error) {
        console.error(error);
      }
    }
    fetchData();
    
  }, [department])

  const handleSubmit = async (e) =>{
    e.preventDefault();
    setOpenPopup(false);
  }
    
    return (
        <Box>
           <form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
             <Grid item md={12}>
             <Box id="siteTitle" mb={3} component="div">
            <TextField
          label="Designation Name"
          variant="outlined"
          fullWidth
        />
           
             </Box>
             {/* end Site Title field */}
             <Box id="siteTitle" mb={3} component="div">
             <Autocomplete
      id="department"
      options={department}
      getOptionLabel={(department) => department.name}
      value={value}
          onChange={(event, newValue) => {
            console.log(newValue);
            if (newValue) {
              setId(newValue._id);
              setValue(newValue);
            }
          }}

      renderInput={(params) => <TextField {...params} label="Department" variant="outlined" />}
    />
           <input type="hidden" name="department_id" value={id} />
             </Box>
             <Box flexWrap="nowrap" alignItems="center" justifyContent="center" display='flex' component="div">  
             <Button variant="contained" className="addBtn submit-btn" type="submit"  size="large" disableElevation >Submit</Button></Box>
             </Grid>
                 
             </Grid>
             </form>
        </Box>
    )
}

export default AddDesignation
