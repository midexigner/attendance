import React,{useState,useEffect} from 'react'
import Layout from '../../components/Layout'
import { makeStyles, createStyles,Paper,Table, TableBody, TableContainer, TableHead,TableFooter, TableRow,TableCell, TablePagination, IconButton } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import axios from "../../services/axios";
import requests from "../../services/requests";
import Popup from '../../components/Popup/Popup'
import DepartmentSingle from '../../components/DepartmentSingle';
import AddDesignation from './AddDesignation'

const Designation = () => {
  const useStyles = makeStyles((theme: Theme) => createStyles({
    miIcon: {
      padding:'0px',
      fontSize:'12px',
      marginLeft:'8px',
      '& svg':{
        fontSize:'16px'
      }
    },
   tableTd:{
      padding:'8px 16px'
    }

  }));
  
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(6);
    const [openPopup, setOpenPopup] = useState(false);
    const [lists,setLists] = useState([{}])
    useEffect(() => {
      async function fetchData() {
        try {
          const request = await axios.get(requests.fetchDesignation);
        setLists(request.data);
          return request.data;
        } catch (error) {
          console.error(error);
        }
      }
  
      fetchData();
  
    }, [])
        const handleChangePage = (event, newPage) => {
          setPage(newPage);
        };
      
        const handleChangeRowsPerPage = (event) => {
          setRowsPerPage(+event.target.value);
          setPage(0);
        };

        const classes = useStyles();
      return (
          <Layout title="Designation" onClick={()=>setOpenPopup(true)} addText="Add Designation">
             <TableContainer component={Paper}>
                 <Table aria-label="custom pagination table">
                 <TableHead>
                     <TableRow>
                        <TableCell variant="head" align="left">#</TableCell>
                         <TableCell variant="head" align="left">Name</TableCell>
                         <TableCell  variant="head" align="left">Department</TableCell>
                         <TableCell variant="head" align="right">Action</TableCell>
                     </TableRow>
                 </TableHead>
                 <TableBody>
                 {lists.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row,index) => (
                   <TableRow key={index}>
                     <TableCell className={classes.tableTd}  variant="body" align="left">{index + 1}</TableCell>
                   <TableCell className={classes.tableTd}  variant="body" align="left">{`${row.name}`}</TableCell>
                   <TableCell className={classes.tableTd}   variant="body" align="left"><DepartmentSingle id={row.department_id} /></TableCell>
                   <TableCell className={classes.tableTd}   variant="body" align="right">
                     <IconButton color="inherit"  className={classes.miIcon}><EditIcon/></IconButton>
                     <IconButton color="inherit" className={classes.miIcon}><DeleteIcon/></IconButton>
                     </TableCell>
               </TableRow>
                 ))}
                 </TableBody>
                 <TableFooter>
                   <TableRow>
                   <TablePagination
          rowsPerPageOptions={[10, 50, 100,999]}
          component="td"
          count={lists.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
                   </TableRow>
                   </TableFooter>
                 </Table>
             </TableContainer>
   <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}  title="Add Designation" onClose={()=>setOpenPopup(false)}>
<AddDesignation />
             </Popup>
          </Layout>
    )
}

export default Designation
